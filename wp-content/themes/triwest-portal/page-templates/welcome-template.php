<?php
/*
    Template Name: Welcome Template
*/

include_once ( get_stylesheet_directory() . "/modules/welcome/src/welcome_page_template.php" );

$welcome = new \triwest_portal\Welcome_Page_Template();
$welcome->create_page();
