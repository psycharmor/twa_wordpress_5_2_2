"use strict";

/*
    The main script file of the registration page
*/

var $j = jQuery.noConflict();

// jquery selectors
var current_window;
var next_btn;
var indicator;
var alert_btn;
var alert_msg;

// ajax
var ajax_url;
var ajax_timeout; // ms

$j( document ).ready( function() {
    /*
        setup the variables that will be used by all script files.
        Also handle the next button click event and the alert events
    */

    current_window = $j( ".form-group.window" ).eq( 0 );
    next_btn = $j( ".pai-btn" );
    indicator = $j( ".pai-step" ).eq( 0 );
    alert_btn = $j( ".alert.alert-danger" );
    alert_msg = $j( ".alert.alert-danger p" );

    ajax_url = window.location.origin + "/wp-admin/admin-ajax.php";
    ajax_timeout = 300000;

    // next_btn.click( function() {
    //     handle_current_window();
    // });
    $j( 'form' ).submit( function(e) {
        e.preventDefault();
        e.returnValue = false;
        next_btn.prop( "disabled", true );

        handle_current_window();
    });

    // close the alert window
    $j( ".alert.alert-danger button" ).click( function() {
        alert_btn.hide();
    });
});

function handle_current_window() {
    /*
        Check which window the user is currently on. Then handle that window
        accordingly.
        @params:
            None
        @return:
            None
    */

    if( current_window.index() == 0 ) {
        handle_verify_window();
    }

    else if( current_window.index() == 1 ) {
        handle_register_window();
    }

    else if( current_window.index() == 2 ) {
        handle_opt_out_window();
    }
}

function show_next_window() {
    /*
        show the next window of the registration form and update the indicators.
        @params:
            None
        @return:
            None
    */

    current_window.removeClass( "active" );

    current_window = current_window.next();
    current_window.addClass( "active" );

    indicator = indicator.next().next(); // skip the hr tag
    indicator.addClass( "active" );
}

function input_field_filled( input ) {
    /*
        Check if the input field given is filled or empty
        @params:
            input       -> selector
        @return:
            bool
    */

    // if the input is empty or contains only whitespaces
    if( !input.val() || !input.val().trim() ) {
        input.addClass( "is-invalid" );
        return false;
    }

    input.removeClass( "is-invalid" );
    return true;
}

function select_field_filled( select ) {
    /*
        Check if the select field given is selected or not
        @params:
            select       -> selector
        @return:
            bool
    */

    if ( select.val() == "0") {
        select.addClass( "is-invalid" );
        return false;
    }

    else {
        select.removeClass( "is-invalid" );
        return true;
    }
}

function value_matches_regex( selector, pattern ) {
    /*
        Check if the value of the selector matches the pattern.
        @params:
            selector            -> selector
            pattern             -> regex
        @return:
            bool
    */

    var value = selector.val();

    if( value.match( pattern ) ) {
        selector.removeClass( "is-invalid" );
        return true;
    }

    else {
        selector.addClass( "is-invalid" );
        return false;
    }
}

function alert_user( msg ) {
    /*
        Show the alert message if the button is clicked and the current form
        is invalid
        @params:
            msg         -> string
        @return:
            None
    */

    alert_msg.text( msg );

    alert_btn.hide();
    alert_btn.fadeIn( 200 );
}
