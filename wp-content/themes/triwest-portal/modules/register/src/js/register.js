"use strict";

/*
    Handle the register window input verification
*/

var registration_fields;

$j( document ).ready( function() {
    /*
        Trigger events related to user input
    */

    $j( "input#email" ).blur( function() {
        // RFC 5322 official standard pattern
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        value_matches_regex( $j( this ), pattern );
    });

    $j( "input#phone-number" ).blur( function() {
        // Simple check
        var pattern = /^[\d\(\)\-\+\s]+$/;
        value_matches_regex( $j( this ), pattern );
    });

    $j( "input#password" ).blur( function( e ) {
        password_is_valid_and_equal( $j( "input#password" ), $j("input#password-confirm"), e.target.id );
    });

    $j( "input#password-confirm" ).blur( function( e ) {
        password_is_valid_and_equal( $j( "input#password" ), $j("input#password-confirm"), e.target.id );
    });

    $j( "input#street-address" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#city" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "select#state" ).change( function() {
        select_field_filled( $j( this ) );
    });

    $j( "select#country" ).change( function() {
        select_field_filled( $j( this ) );
    });

    $j( "input#postal-code" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#discipline" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#organization-name" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#department" ).blur( function() {
        input_field_filled( $j( this ) );
    });

    $j( "input#title" ).blur( function() {
        input_field_filled( $j( this ) );
    });
});

function autofill_inputs() {
    /*
        Using the registration_fields, autofill any values for the register
        window
        @params:
            None
        @return:
            None
    */

    if( "phone_number" in registration_fields ) {
        $j( "input#phone-number" ).val( clean_phone_number( registration_fields["phone_number"], false ) );
    }

    if( "street_address" in registration_fields ) {
        $j( "input#street-address" ).val( registration_fields["street_address"] );
    }

    if( "city" in registration_fields ) {
        $j( "input#city" ).val( registration_fields["city"] );
    }

    if( "state" in registration_fields ) {
        $j( "select#state" ).val( registration_fields["state"] );
    }

    if( "country_code" in registration_fields ) {
        $j( "select#country" ).val( registration_fields["country_code"] );
    }

    if( "postal_code" in registration_fields ) {
        $j( "input#postal-code" ).val( registration_fields["postal_code"] );
    }

    if( "organization_name" in registration_fields ) {
        $j( "input#organization-name" ).val( registration_fields["organization_name"] );
    }

    if( "title" in registration_fields ) {
        $j( "input#title" ).val( registration_fields["title"] );
    }

    if( "taxonomy_code_pri" in registration_fields ) {
        $j( "input#taxonomy-code-pri" ).val( registration_fields["taxonomy_code_pri"] );
    }

    if( "taxonomy_desc_pri" in registration_fields ) {
        $j( "input#taxonomy-desc-pri" ).val( registration_fields["taxonomy_desc_pri"] );
    }

    if( "taxonomy_code_sec" in registration_fields ) {
        $j( "input#taxonomy-code-sec" ).val( registration_fields["taxonomy_code_sec"] );
    }

    if( "taxonomy_desc_sec" in registration_fields ) {
        $j( "input#taxonomy-desc-sec" ).val( registration_fields["taxonomy_desc_sec"] );
    }

    // hide the secondary taxonomy fields
    if( !$j( "input#taxonomy-code-sec" ).val() || !$j( "input#taxonomy-code-sec" ).val().trim() ) {
        $j( "#secondary-taxonomy-fields" ).hide();
    }
}

function handle_register_window() {
    /*
        Handles 2 part check of this window
        1. Check if the fields are valid
        2. Use the fields and create a new user
        @params:
            None
        @return:
            None
    */

    if( register_window_fields_are_valid() ) {
        $j( "body" ).css( "cursor", "wait" );
        handle_register_window_api( function( response ) {
            $j( "body" ).css( "cursor", "" );
            if( response == -1 ) {
                alert_user( "Email already exists. Please try a different email." );
            }

            else if (response == 0) {
                alert_btn.hide();
                next_btn.prop( "disabled", false );
                show_next_window();
            }
        });
    }

    else {
        next_btn.prop( "disabled", false );
    }
}

function handle_register_window_api( callback ) {
    /*
        Make an ajax call using the fields from the register window.
        Used to create a new user with the fields given or alert the user
        if the user/email already exists
        @params:
            callback            -> function
        @return:
            None
    */

    var data = {
        "action":               "triwest_portal_create_user",
        "email":                $j( "input#email" ).val().trim(),
    };

    $j.ajax( {
        type:       "POST",
        url:        ajax_url,
        data:       data,
        timeout:    ajax_timeout,
        success:    callback,
        error:      function() {
            $j( "body" ).css( "cursor", "" );
            alert_user( "Request timeout. Please try registering again." );
            next_btn.prop( "disabled", false );
        }
    });
}

function register_window_fields_are_valid() {
    /*
        Verify if all the user input fields are valid
        @params:
            None
        @return:
            None
    */

    var none_invalid = true;

    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( !value_matches_regex( $j( "input#email" ), pattern ) ) {
        none_invalid = false;
    }

    pattern = /^[\d\(\)\-\+\s]+$/;
    if( !value_matches_regex( $j( "input#phone-number" ), pattern ) ) {
        none_invalid = false;
    }

    if( !password_is_valid_and_equal( $j( "input#password" ), $j( "input#password-confirm" ), "password" ) ) {
        none_invalid = false;
    }

    if( !password_is_valid_and_equal( $j( "input#password" ), $j( "input#password-confirm" ), "password-confirm" ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#street-address" ) ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#city" ) ) ) {
        none_invalid = false;
    }

    if( !select_field_filled( $j( "select#state" ) ) ) {
        none_invalid = false;
    }

    if( !select_field_filled( $j( "select#country" ) ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#postal-code" ) ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#discipline" ) ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#organization-name" ) ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#department" ) ) ) {
        none_invalid = false;
    }

    if( !input_field_filled( $j( "input#title" ) ) ) {
        none_invalid = false;
    }

    return none_invalid;
}

function clean_phone_number( phone_number, only_digits ) {
    /*
        Use regex to remove either all non digits from the phone number
        or all nondigits except dashes "-"
        @params:
            phone_number            -> string
            only_digits             -> bool
        @return:
            string
    */

    if( only_digits ) {
        return phone_number.replace( /\D/g, '' );
    }

    return phone_number.replace( /[^\d\-]/g, '' );
}

function password_is_valid_and_equal( password, password_confirm, target ) {
    /*
        Check if the password is valid with the following requirements:
            -   minimum 6 characters
            -   no spaces
            -   minimum 1 capital letter
            -   minimum 1 lowercase letter
            -   minimum 1 digit
            -   confirmation password must match password
        @params:
            password            -> selector
            password_confirm    -> selector
            target              -> string
        @return:
            bool
    */

    var pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;

    if( target == "password" ) {
        // if password-confirm is empty
        if( !password_confirm.val() || !password_confirm.val().trim() ) {
            value_matches_regex( password, pattern );

            // either way, return false because password does not match confirmation
            return false;
        }

        else {

            // check if password meets requirements
            if( !value_matches_regex( password, pattern ) ) {
                return false;
            }

            return passwords_are_equal( password, password_confirm );
        }
    }

    else if( target == "password-confirm" ) {

        // check if password meets requirements
        if( !value_matches_regex( password, pattern ) ) {
            return false;
        }

        return passwords_are_equal( password, password_confirm );
    }
}

function passwords_are_equal( password, password_confirm ) {
    /*
        Check if the password and the confirmation are equal
        @params:
            password            -> selector
            password_confirm    -> selector
        @return:
            bool
    */

    if ( !( password.val() == password_confirm.val() ) ) {
        password_confirm.addClass( "is-invalid" );
        return false;
    }

    else {
        password.removeClass( "is-invalid" );
        password_confirm.removeClass( "is-invalid" );
        return true;
    }
}
