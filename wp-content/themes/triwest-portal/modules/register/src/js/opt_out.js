"use strict";

/*
    Handle the opt out window check box selection
*/

$j( document ).ready( function() {
    /*
        Handle the events of either checkbox being selected
        When clicked:
            if checked:
                if other also checked -> opt out both
                if other not checked  -> opt out this
            if not checked:
                if other checked      -> opt out other
                if other not checked  -> enroll
    */

    $j( ".pai-checkbox-container input#opt-out-opioid" ).click( function() {
        handle_opt_out_is_clicked( $j( this ) );
    });

    $j( ".pai-checkbox-container input#opt-out-gcs" ).click( function() {
        handle_opt_out_is_clicked( $j( this ) );
    });

    $j( ".pai-checkbox-container input#opt-out-none" ).click( function() {
        handle_opt_out_is_clicked( $j( this ) );
    });
});

function handle_opt_out_window() {
    /*
        Set up the fields for the registration form
        @params:
            None
        @return:
            None
    */

    // ( (opioid OR gcs) XOR None )
    if( ( $j( ".pai-checkbox-container input#opt-out-opioid" ).is( ":checked" ) || $j( ".pai-checkbox-container input#opt-out-gcs" ).is( ":checked" ) ) != $j( ".pai-checkbox-container input#opt-out-none" ).is( ":checked" ) ) {

        var phone_number = clean_phone_number( $j( "input#phone-number" ).val().trim(), true );
        $j( "input#phone-number" ).val( phone_number );

        $j( "input#taxonomy-code-pri" ).prop( "disabled", false );
        $j( "input#taxonomy-desc-pri" ).prop( "disabled", false );
        if( $j( "input#taxonomy-code-sec" ).val() && $j( "input#taxonomy-code-sec" ).val().trim() ) {
            $j( "input#taxonomy-code-sec" ).prop( "disabled", false );
            $j( "input#taxonomy-desc-sec" ).prop( "disabled", false );
        }

        var current_datetime = ( new Date ).toString();
        var time_zone = current_datetime.substr( current_datetime.indexOf( "GMT" ) );
        $j( "input#time-zone" ).val(time_zone);

        $j( "form" ).off( "submit" );
        $j( "form" ).submit();
    }

    else {
        alert_user( "Please check one or both of the opt out checkboxes OR the \"Continue to VA-Required Training\" checkbox" );
        next_btn.prop( "disabled", false );
    }
}

function handle_opt_out_is_clicked( selector ) {
    /*
        Handle the case where any of the opt out courses are clicked
        @params:
            selector            -> selector
        @return:
            None
    */

    if( selector.is( ".pai-checkbox-container input#opt-out-none" ) ) {
        if( $j( ".pai-checkbox-container input#opt-out-none" ).is( ":checked" ) ) {
            $j( ".pai-checkbox-container input#opt-out-opioid" ).prop( "checked", false );
            $j( ".pai-checkbox-container input#opt-out-gcs" ).prop( "checked", false );
        }
    }

    else if ( selector.is( ".pai-checkbox-container input#opt-out-opioid" ) || selector.is( ".pai-checkbox-container input#opt-out-gcs" ) ) {
        $j( ".pai-checkbox-container input#opt-out-none" ).prop( "checked", false );
    }
}

function change_next_button( msg, label, style ) {
    /*
        Change the ending message, the button label, and the button's style
        @params:
            msg             -> string
            label           -> string
            style           -> json
    */

    next_btn.text( label );
    next_btn.css( style );
}
