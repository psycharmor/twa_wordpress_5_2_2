<?php
/*
    Name: check_nppes.php
    Description:
        Hold the function that handles the ajax call to verify whether the
        healthcare provider exists in the NPPES database given user input

        Exists in NPPES: Give json response that was obtained
        Exists in PAI database: Give back json with url to login
        Not exists: Give back -1
*/

function ajax_check_nppes() {
    /*
        checks whether the user exists in the NPPES database or not given fields
        @params: (from ajax request)
            npi_number          -> string
            first_name          -> string
            last_name           -> string
        @return:
            none
    */

    if( isset( $_REQUEST ) ) {
        // first check if the npi number exists in our database
        if( npi_number_exists( sanitize_text_field( intval($_REQUEST["npi_number"]) ) ) ) {
            $fields = array( "url" => wp_login_url( "/my-dashboard/" ) );
            wp_send_json( json_encode( $fields ) );
        }

        // make a GET request to NPPES API
        // $npi_url = "https://npiregistry.cms.hhs.gov/api/?version=2.1" .
        //     "&number=" . sanitize_text_field($_REQUEST['npi_number']) .
        //     "&first_name=" . sanitize_text_field($_REQUEST['first_name']) .
        //     "&last_name=" . sanitize_text_field($_REQUEST['last_name']);

        $npi_url = "https://npiregistry.cms.hhs.gov/api/?version=2.1" .
            "&number=" . sanitize_text_field($_REQUEST['npi_number']);

        $response = wp_remote_get( $npi_url, array( "timeout" => 30 ) );

        if( is_wp_error( $response ) ) {
            echo -2;
            wp_die();
        }

        $response = json_decode( $response['body'], true );

        if( array_key_exists( "result_count", $response ) && $response["result_count"] > 0 ) {
            // Send relevant data back to caller
            $fields = parse_response( $response );

            wp_send_json( json_encode( $fields ) ); // wp_die()
        }

        else {
            echo -1;
            wp_die();
        }
    }

    else {
        echo -1;
        wp_die();
    }
}

function npi_number_exists( $npi_number ) {
    /*
        Check the pai database if the npi_number exists
        @params:
            npi_number          -> string
        @return:
            bool
    */

    $args = array(
        "meta_key"      => "npi_number",
        "meta_value"    => $npi_number,
        "meta_compare"  => "="
    );

    $users = get_users( $args );

    return  !empty( $users );
}

function parse_response( $response ) {
    /*
        parse the json response to get only the relevant fields for registration
        @params:
            $response               -> array
        @return:
            array
    */

    $data = $response["results"][0];
    $relevant_data = array();

    // gather data from addresses result
    if( array_key_exists( "addresses", $data ) ) {
        if( array_key_exists( "address_1", $data["addresses"][0] ) ) {
            $relevant_data["street_address"] = $data["addresses"][0]["address_1"];
        }

        if( array_key_exists( "city", $data["addresses"][0] ) ) {
            $relevant_data["city"] = $data["addresses"][0]["city"];
        }

        if( array_key_exists( "state", $data["addresses"][0] ) ) {
            $relevant_data["state"] = $data["addresses"][0]["state"];
        }

        if( array_key_exists( "country_code", $data["addresses"][0] ) ) {
            $relevant_data["country_code"] = $data["addresses"][0]["country_code"];
        }

        if( array_key_exists( "postal_code", $data["addresses"][0] ) ) {
            $postal_code = $data["addresses"][0]["postal_code"];
            $relevant_data["postal_code"] = substr( $postal_code, 0, 5 );
        }

        if( array_key_exists( "telephone_number", $data["addresses"][0] ) ) {
            $relevant_data["phone_number"] = $data["addresses"][0]["telephone_number"];
        }
    }

    // gather data from taxonomies result
    if( array_key_exists( "taxonomies", $data ) ) {
        $secondary_filled = false;
        foreach( $data["taxonomies"] as $taxonomy ) {
            if( $taxonomy["primary"] ) {
                $relevant_data["taxonomy_code_pri"] = $taxonomy["code"];
                $relevant_data["taxonomy_desc_pri"] = $taxonomy["desc"];
            }

            else if( !$secondary_filled ) {
                $secondary_filled = true;
                $relevant_data["taxonomy_code_sec"] = $taxonomy["code"];
                $relevant_data["taxonomy_desc_sec"] = $taxonomy["desc"];
            }
        }
    }

    // gather data from basic result
    if( array_key_exists( "basic", $data ) ) {
        if( array_key_exists( "authorized_official_title_or_position", $data["basic"] ) ) {
            $relevant_data["title"] = $data["basic"]["authorized_official_title_or_position"];
        }

        if( array_key_exists( "organization_name", $data["basic"] ) ) {
            $relevant_data["organization_name"] = $data["basic"]["organization_name"];
        }
    }

    return $relevant_data;
}

add_action("wp_ajax_nopriv_triwest_portal_check_nppes", "ajax_check_nppes");   // user not logged
add_action("wp_ajax_triwest_portal_check_nppes", "ajax_check_nppes");          // user logged in
