<?php
/*
    Name: register_page_template.php
    Description:
        Extend the base page template to create the registration page
*/

namespace triwest_portal;

include_once ( get_stylesheet_directory() . "/modules/register/src/php/register_body.php" );
include_once ( get_stylesheet_directory() . "/modules/register/src/php/validation_window.php" );
include_once ( get_stylesheet_directory() . "/modules/register/src/php/register_window.php" );
include_once ( get_stylesheet_directory() . "/modules/register/src/php/opt_out_window.php" );
include_once ( get_stylesheet_directory() . "/modules/src/php/page_template.php" );

class Register_Page_Template extends Page_Template {

    private $register_body;
    private $windows;

    public function __construct() {
        /*
            Setup all the member variables of the class
            @params:
                None
            @return:
                None
        */

        $this->register_body = new Register_Body();
        $this->windows = array(
            new Validation_Window(),
            new Register_Window(),
            new Opt_Out_Window()
        );
    }

    private function include_files() {
        /*
            Include all the stylesheets and scripts needed for the register page
            @params:
                None
            @return:
                None
        */

        $content = "<!-- FROM THE REGISTER TEMPLATE -->";

        $content .= "<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/modules/register/src/js/base_register.js'></script>";
        $content .= "<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/modules/register/src/js/verify.js'></script>";
        $content .= "<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/modules/register/src/js/register.js'></script>";
        $content .= "<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/modules/register/src/js/opt_out.js'></script>";

        $content .= "<link type='text/css' rel='stylesheet' href='" . get_stylesheet_directory_uri() . "/modules/register/src/css/style.css'>";

        return $content;
    }

    private function create_head_element() {
        /*
            Create the <head> element
            @params:
                None
            @return:
                string
        */

        $title = "Sign Up - " . get_bloginfo( "name" );

        $content = "";

        $content .= "<head>";
            $content .= "<title>$title</title>";
            $content .= $this->include_base_files();
            $content .= $this->include_files();
        $content .= "</head>";

        return $content;
    }

    private function create_body_element() {
        /*
            Create the <body> element
            @params:
                None
            @return:
                string
        */
        $form_url = esc_url( admin_url( "admin-post.php" ) );

        $content = "";

        $content .= "<body class='pai-bg-img'>";
            $content .= "<div class='pai-page'>";
                $content .= "<form action='$form_url' method='post' autocomplete='off'>";
                    $content .= $this->create_header();
                    $content .= $this->register_body->create_alert_message();
                    $content .= "<div class='pai-signup-form'>";
                        foreach( $this->windows as $window ) {
                            $content .= $window->create_window();
                        }
                    $content .= "</div>"; // pai-signup-form
                    $content .= "<input type='hidden' name='action' value='register_form'>";
                    $content .= $this->register_body->create_btn( "Verify" );
                    $content .= $this->register_body->create_indicators( 3 );
                $content .= "</form>";
            $content .= "</div>"; // pai-page
            $content .= $this->create_footer();
        $content .= "</body>";

        return $content;
    }

    public function create_page() {
        /*
            Create the entire page, including the <head> and the <body>
            @params:
                None
            @return:
                None
        */

        $content = "";

        $content .= $this->create_head_element();
        $content .= $this->create_body_element();

        echo html_entity_decode( $content );
    }
}
