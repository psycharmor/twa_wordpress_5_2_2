<?php
/*
    Name: certificates.php
    Description:
        Create the class responsible for creating the dashboard's certificate lists
*/

namespace triwest_portal;

class Dashboard_Certificates {

    private $user;

    public function __construct( $user ) {
        /*
            Set the class's member variables
            @params:
                $user           -> WP_User
        */

        $this->user = $user;
    }

    private function get_certificates( $user ) {
        /*
            Get all of the user certificates as an array
            @params:
                $user           -> WP_User
            @return:
                array( $title => $link )
        */

        $certs = array();

        $course_ids = ld_get_mycourses( $user->ID );
        // foreach( $course_ids as $course_id ) {
        //     $cert = pai_get_course_certificate_link( $course_id, $user->ID );
        //     if( !empty( $cert ) ) {
        //         $certs[get_the_title( $course_id )] = $cert;
        //     }
        // }

        for( $i = count( $course_ids ) - 1; $i >= 0; --$i ) {
            $course_id = $course_ids[ $i ];
            $cert = pai_get_course_certificate_link( $course_id, $user->ID );
            if( !empty( $cert ) ) {
                $certs[get_the_title( $course_id )] = $cert;
            }
        }

        return $certs;
    }

    private function create_certificate_element_header( $number ) {
        /*
            Create the header element text that shows before each
            certificate download box
            @params:
                $number         -> int
            @return:
                string
        */

        $header = "Veteran Ready Healthcare Provider Certificate (Course #$number) for:";

        $content = "";

        $content .= "<p class='pai-dashboard-text'>";
            $content .= $header;
        $content .= "</p>"; // pai-dashboard-text

        return $content;
    }

    private function create_certificate_element( $title, $link ) {
        /*
            Create the element that holds the certificate download links
            @params:
                $title          -> string
                $link           -> string
            @return:
                string
        */

        $content = "";

        $content .= "<a class='pai-dashboard-certificate-box' href='$link' target='_blank'>";
            $content .= "<i class='fa fa-arrow-circle-down pai-dashboard-download-btn'></i>";
            $content .= "<p class='pai-dashboard-text-title'>\"$title\"</p>";
        $content .= "</a>";

        return $content;
    }

    public function create_section() {
        /*
            Create the certificate section of the dashboard
            @params:
                None
            @return:
                string
        */

        $certs = $this->get_certificates( $this->user );

        $content = "";

        if( !empty( $certs ) ) {
            $content .= "<p class='pai-dashboard-text bold'>Download</p>";
        }

        $count = 0;
        foreach( $certs as $title => $link ) {
            $content .= $this->create_certificate_element_header( ++$count );
            $content .= $this->create_certificate_element( $title, $link );
        }

        return $content;
    }
}
