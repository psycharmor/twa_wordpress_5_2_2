<?php
/*
    Name: profile.php
    Description:
        Create the class responsible for creating the dashboard's user profile
*/

namespace triwest_portal;

class Dashboard_Profile {

    private $user;

    public function __construct( $user ) {
        /*
            Set the class's member variables
            @params:
                $user           -> WP_User
        */

        $this->user = $user;
    }

    /* UTILITY FUNCTION DELETE ON PRODUCTION */
    private function reset_user_profile($user )
    {
      session_destroy();

      $content = '';

        $content .= '<br>';
        $content .= '<button>';
          $content .= do_shortcode( '[plugin_delete_me /]' );
          $content .= '</button>';
        $content .= '<br>';

      return $content;

    }
    /* END UTILITY FUNCTION */

    // private function create_avatar() {
    //     /*
    //         Create the avatar element of the section. Holds the user's profile
    //         picture.
    //         @params:
    //             None
    //         @return:
    //             string
    //     */
    //
    //     $content = "";
    //
    //     $content .= "<div class='pai-dashboard-profile-img-container'>";
    //         $content .= get_avatar( $this->user->ID, $size = "120" );
    //         $content .= $this->reset_user_profile($user); // DELETE DIS
    //     $content .= "</div>"; // pai-dashboard-profile-img
    //
    //     return $content;
    // }

    private function get_meta( &$user_meta, $meta_key ) {
        /*
            Give back the meta value of the meta key or an empty string
            @params:
                $user_meta          -> array
                $meta_key           -> string
            @return:
                string
        */

        if( empty( $user_meta[$meta_key] ) ) {
            return "";
        }

        return $user_meta[$meta_key][0];
    }

    private function get_specialty( $taxonomies ) {
        /*
            Get the description of the primary taxonomy
            @params:
                $taxonomies             -> array
            @return:
                string
        */

        foreach( $taxonomies as $taxonomy ) {
            if( $taxonomy["primary"] === true ) {
                return $taxonomy["description"];
            }
        }

        return "";
    }

    private function create_label_and_value( $label, $value ) {
        /*
            Create an element which consists of a label value pair
            @params:
                $label          -> string
                $value          -> string
            @return:
                string
        */

        $content = "";

        // $content .= "<div class='pai-dashboard-profile-info-container'>";
        //     $content .= "<p class='pai-dashboard-text bold'>$label</p>";
        //     $content .= "<p class='pai-dashboard-text value'>$value</p>";
        // $content .= "</div>"; // pai-dashboard-profile-info-container

        $content .= "<dl class='row'>";
            $content .= "<dt class='col-sm-5 pai-dashboard-text bold'>$label</dt>";
            $content .= "<dd class='col-sm-7 pai-dashboard-text'>$value</dd>";
        $content .= "</dl>"; // pai-dashboard-profile-info-container

        return $content;
    }

    private function create_profile_info() {
        /*
            Create the profile section that displays user meta information
            @params:
                None
            @return:
                string
        */

        $user_meta = get_user_meta( $this->user->ID );

        $first_name = $this->get_meta( $user_meta, "first_name" );
        $last_name = $this->get_meta( $user_meta, "last_name" );

        $full_name = strtoupper( $first_name . " " . $last_name );
        $discipline = $this->get_meta( $user_meta, "discipline" );

        $npi_number = $this->get_meta( $user_meta, "npi_number" );

        $specialty = "";
        if( isset( $user_meta["taxonomies"] ) ) {
            $specialty = $this->get_specialty( unserialize( $user_meta["taxonomies"][0] ) );
        }

        $content = "";

        $content .= "<h1 class='pai-dashboard-profile-name'>$full_name</h1>";
        $content .= "<p class='pai-dashboard-text'>$discipline</p>";
        $content .= "<hr class='pai-dashboard-profile-sep'>";
        $content .= $this->create_label_and_value( "NPI-1 Number", $npi_number );
        $content .= $this->create_label_and_value( "PsychArmor ID", $this->user->ID );
        $content .= $this->create_label_and_value( "Provider Specialty", $specialty );
        $content .= "<hr class='pai-dashboard-profile-sep'>";

        return $content;
    }

    public function create_section() {
        /*
            Create the profile section of the dashboard
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= $this->create_profile_info();

        return $content;
    }

}
