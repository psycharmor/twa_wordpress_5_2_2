<?php
/*
    Name: enroll_user.php
    Description:
        Hold the function that handles the ajax call to send the user to the
        course page. But first checking if they are logged in before enrolling
        them into the course if they are not already.

        Not Logged in: require log in
        Not Enrolled: Enroll user to course before sending them to course
        Enrolled: send user to course only
*/

function ajax_enroll_user() {
    /*
        Sends the logged in user to the course, enrolling them if they are not
        already.
        @params: (from ajax request)
            course_id          -> string
        @return:
            none
    */

    if( isset( $_REQUEST ) ) {
        $course_id = intval( sanitize_text_field( $_REQUEST["course_id"] ) );
        $user = wp_get_current_user();

        // if user not logged; can happen if they log out but backs to the dashboard
        if( $user->ID === 0 ) {
            send_user_to_url( wp_login_url(), "_self" );
        }

        else {
            // enroll the user to the course first if not already
            $user_course_list = ld_get_mycourses( $user->ID );
            if( !in_array( $course_id, $user_course_list ) ) {
                ld_update_course_access( $user->ID, $course_id );
            }

            send_user_to_course( $course_id );
        }
    }
}

function send_user_to_course( $course_id ) {
    /*
        Get the link of the course_id which depends if the course is a
        train course or not
        @params:
            $course_id          -> int
        @return:
            None
    */

    $terms = wp_get_post_terms( $course_id, "ld_course_tag" );
    foreach( $terms as $term ) {
        if( $term->name === "train_course" ) {
            $url = get_post_meta( $course_id, "train_course_link", true );
            send_user_to_url( $url, "_blank" );
        }
    }

    // not a train course
    $course_lesson = learndash_get_course_lessons_list( $course_id )[1]["permalink"];
    send_user_to_url( $course_lesson, "_self" );
}

function send_user_to_url( $url, $target ) {
    /*
        Send the user to the url based on the target
        @params:
            $url        -> string
            $target     -> string
        @return:
            None
    */

    $url_array = array(
        "url"    => $url,
        "target" => $target
    );
    wp_send_json( json_encode( $url_array ) );
}

add_action("wp_ajax_nopriv_triwest_portal_enroll_user", "ajax_enroll_user");   // user not logged
add_action("wp_ajax_triwest_portal_enroll_user", "ajax_enroll_user");      // user logged in
