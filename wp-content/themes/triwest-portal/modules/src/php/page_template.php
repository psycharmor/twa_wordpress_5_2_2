<?php
/*
    Name: page_template.php
    Description:
        Create the base template class of all the pages
*/

namespace triwest_portal;

class Page_Template {

    protected function include_base_files() {
        /*
            Create all the script and stylesheet html elements that are needed for
            all page templates.
            @params:
                None
            @return:
                string
        */

        $content = "<!-- FROM THE BASE TEMPLATE -->";

        // Responsiveness
        $content .= "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";

        // external libraries
        $content .= "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>";
        $content .= "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>";
        $content .= "<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>";
        $content .= "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";

        // internal libraries
        $content .= "<link type='text/css' rel='stylesheet' href='" . get_stylesheet_directory_uri() . "/modules/src/css/style.css'>";

        return $content;
    }

    protected function create_header() {
        /*
            Create the html elements that make up the header of all the pages.
            @params:
                None
            @return:
                string
        */

        $url = home_url();
        if( is_user_logged_in() ) {
            $url = home_url( "/my-dashboard/" );
        }

        $content = "";

        $content .= "<div class='pai-header-template'>";
            $content .= "<a href='" . $url ."'>";
                $content .= "<img src='" . home_url( "/wp-content/uploads/2019/08/Logo-4-Header-PA-2-1.png" ) . "'>";
            $content .= "</a>";
        $content .= "</div>"; // pai-header-template

        return $content;
    }

    public function create_header_img() {
        /*
            Add that god forsaken img from TriWest. Who thought it
            was a good idea?
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<div class='pai-header-img'>";
            $content .= "<img src='" . home_url( "/wp-content/uploads/2019/08/Rectangle-2.jpg" ) . "'>";
        $content .= "</div>"; // pai-header-img

        return $content;
    }

    protected function create_footer() {
        /*
            Create the footer for each page
            TODO: clean this later. I'm over it
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<div class='pai-blue-border pai-footer-container'>";
            $content .= "<ul class='pai-footer'>";
                $content .= "<li>";
                    $content .= "Support Center: Monday - Friday 7:00 am - 4:00 pm PST";
                $content .= "</l1>";
                $content .= "<li>";
                    $content .= "<i class='fa fa-phone pai-footer-icon'></i>";
                    $content .= "1-858-755-3006";
                $content .= "</l1>";
                $content .= "<li>";
                    $content .= "<i class='fa fa-envelope pai-footer-icon'></i>";
                    $content .= "triwestsupport@domain.org";
                $content .= "</l1>";
                $content .= "<li>";
                    $content .= "Copyright © 2019 - TriWest Healthcare Alliance";
                $content .= "</l1>";
            $content .= "</ul>";
        $content .= "</div>"; // pai-blue-border

        return $content;
    }
}
