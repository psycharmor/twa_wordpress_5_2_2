<?php
/*
    Name: triwest_api_controller.php
    Description:
        Hold the class that creates the PsychArmor -> TriWest API
        while extends from WP Rest API
*/

class Triwest_Api_Controller extends WP_Rest_Controller {

    private $limit;
    private $offset;
    private $start_date;
    private $end_date;
    private $basic_required_fields;
    protected $namespace;

    public function __construct() {
        /*
            Setup all the member variables of the class
            @params:
                None
            @return:
                None
        */

        $this->namespace = "triwest/v1";
        $this->basic_required_fields = array(
            "first_name",
            "last_name",
            "phone_number",
            "npi_number",
            "street_address",
            "city",
            "state",
            "country",
            "postal_code",
            "time_zone",
            "discipline",
            "organization",
            "department",
            "title"
        );
    }

    private function create_date( $date ) {
        /*
            Create a datetime object given a date format
            @params:
                $date           -> string
            @return:
                DateTime/string
        */

        // check if given date is unix epoch time
        if( is_numeric( $date ) ) {
            return (new DateTime( "@$date" ))->getTimestamp();
        }

        else {
            try {
                // get date then convert to epoch
                return (new DateTime( $date ))->getTimestamp();
            }
            catch( Exception $e ) {
                return "";
            }
        }
    }

    private function prepare_sql_filters( &$request ) {
        /*
            Check all the wpdb based filters to see if they are valid and set them
            to the member variables. If not, return an error
            @params: (from $request)
                limit           -> string
                start_date      -> string
                end_date        -> string
            @return:
                None/WP_Error
        */
        $this->start_date = 0;
        $this->end_date = time();
        $this->limit = 250;

        if( isset( $request["limit"] ) ) {
            $this->limit = $request["limit"];
            if ( !is_numeric( $this->limit ) ) {
                return new WP_Error("invalid_argument", "limit contains an invalid argument");
            }
            $this->limit = intval( $this->limit );
        }

        if( isset( $request["offset"] ) ) {
            $this->offset = $request["offset"];
            if ( !is_numeric( $this->offset ) ) {
                return new WP_Error("invalid_argument", "offset contains an invalid argument");
            }
            $this->offset = intval( $this->offset );
        }

        if ( isset( $request["start_date"] ) ) {
            $this->start_date = $this->create_date( $request["start_date"] );
            if ( empty( $this->start_date ) ) {
                return new WP_Error("invalid_argument", "start_date contains an invalid argument");
            }
        }

        if ( isset( $request["end_date"] ) ) {
            $this->end_date = $this->create_date( $request["end_date"] );
            if ( empty( $this->end_date ) ) {
                return new WP_Error("invalid_argument", "end_date contains an invalid argument");
            }
        }
    }

    private function get_learndash_activity( &$request, $user_id ) {
        /*
            Query the learndash_user_activity database based on queries set by
            the url based on user
            @params:
                $request        -> array
                $user_id        -> int
            @return:
                array
        */
        $query_array = array(
            $user_id,
            $this->start_date,
            $this->end_date,
        );

        // $limit_query = "";
        // if( isset( $this->limit ) ) {
        //     $query_array[] = $this->limit;
        //     $limit_query = "LIMIT %d ";
        // }

        global $wpdb;

        $query = $wpdb->prepare("
            SELECT DISTINCT user_id, course_id, activity_status, activity_started, activity_completed
            FROM wp_learndash_user_activity
            WHERE activity_type = 'course'
            AND user_id = %s
            AND activity_completed IS NOT NULL
            AND activity_completed > %d
            AND activity_completed <= %d",
        $query_array);

        $results = $wpdb->get_results( $query, ARRAY_A );

        return $results;
    }

    private function create_users_filter( $key, $value ) {
        /*
            create a filter for get_users
            @params:
                $key            -> string
                $value          -> string
            @return:
                array
        */
        if ( !isset( $value ) ) {
            return array();
        }

        return array(
            "key"       => $key,
            "value"     => $value,
            "compare"   => "="
        );
    }

    private function set_users_filters( &$request ) {
        /*
            set the filters to be used to fetch the users
            @params: (from $request)
                npi_number          -> string
                state               -> string
                first_name          -> string
                last_name           -> string
                postal_code         -> string
            @return:
                array
        */
        $keys = array(
            "npi_number"    => $request["npi_number"],
            "first_name"    => $request["first_name"],
            "last_name"     => $request["last_name"],
            "state"         => $request["state"],
            "postal_code"   => $request["postal_code"]
        );
        $filters = array();
        foreach( $keys as $key => $value ) {
            $filter = $this->create_users_filter( $key, $value );
            if( !empty( $filter ) ) {
                $filters[] = $filter;
            }
        }

        return $filters;
    }

    private function get_last_login_time( &$tokens ) {
        /*
            parse the session token and get the last login time of the user
            @params:
                $tokens             -> array
            @return:
                None
        */

        $latest = 0;
        if( !empty( $tokens ) ) {
            foreach( $tokens as $key => $fields ) {
                $login = intval( $fields["login"] );
                if( $login > $latest ) {
                    $latest = $login;
                }
            }
        }

        return ( new DateTime( "@$latest" ) )->format( "F j, Y H:i:s" );
    }

    private function get_serialized_user_data( &$req_user_data, $key, &$serialized ) {
        /*
            unserialize the user meta field, but if it's not available don't
            add it to req_user_data
            @params:
                $req_user_data              -> array
                $key                        -> string
                $serialized                 -> string
            @return:
                None
        */

        $unserialized = unserialize( $serialized );
        if( $unserialized !== false && !empty( $unserialized ) ) {
            $req_user_data[$key] = $unserialized;
        }
    }

    private function get_basic_user_data( &$req_user_data, &$user, $last_login ) {
        /*
            Update req_user_data to get the string based user data
            @params:
                $req_user_data          -> array
                $user                   -> WP_User
                $last_login             -> string
            @return:
                None
        */

        $user_meta = null;
        $user_meta = get_user_meta( $user->ID );

        $req_user_data["id"] = ( !empty( $user->ID ) ? $user->ID : null );
        $req_user_data["login_name"] = ( !empty( $user->user_login ) ? $user->user_login : null );
        $req_user_data["email"] = ( !empty( $user->user_email ) ? $user->user_email : null );
        $req_user_data["last_login"] = ( strtotime( $last_login ) != 0 ? $last_login : null );

        foreach( $this->basic_required_fields as $field ) {
            if ( isset( $user_meta[$field] ) ) {
                $req_user_data[$field] = ( !empty( $user_meta[$field][0] ) ? $user_meta[$field][0] : null );
            }

            else {
                $req_user_data[$field] = null;
            }
        }

        $this->get_serialized_user_data( $req_user_data, "taxonomies", $user_meta["taxonomies"][0] );
        $this->get_serialized_user_data( $req_user_data, "opt_outs", $user_meta["opt_outs"][0] );
    }

    private function get_course_user_data( &$req_user_data, &$user_activity ) {
        /*
            Get the gathered user course activity and add the necessary fields
            to the api
            @params:
                $req_user_data              -> array
                $user_activity              -> array
            @return:
                None
        */

        $courses = array();
        foreach( $user_activity as $activity ) {
            $started = intval( $activity["activity_started"] );
            $completed = intval( $activity["activity_completed"] );

            $course = null;
            $course = array(
                "id"                        => intval( $activity["course_id"] ),
                "title"                     => get_the_title( intval( $activity["course_id"] ) ),
                "completed"                 => boolval( $activity["activity_status"] ),
                "date_started"              => $started,
                "date_completed"            => $completed,
                "date_started_formatted"    => ( new DateTime( "@$started" ) )->format( "F j, Y H:i:s" ),
                "date_completed_formatted"  => ( new DateTime( "@$completed" ) )->format( "F j, Y H:i:s" )
            );

            $courses[] = $course;
        }

        if( !empty( $courses ) ) {
            $req_user_data["courses"] = $courses;
        }
    }

    private function get_certificate_user_data( &$req_user_data, &$user ) {
        /*
            Get the gathered user's earned certificates and
            add the necessary fields to the api
            @params:
                $req_user_data              -> array
                $user                       -> WP_User
            @return:
                None
        */

        $certs = array();
        $course_ids = ld_get_mycourses( $user->ID );

        foreach( $course_ids as $course_id ) {
            $cert_url = pai_get_course_certificate_link( $course_id, $user->ID );
            if( !empty( $cert_url ) ) {
                $cert_id = intval( learndash_get_setting( $course_id, "certificate" ) );
                $course = array(
                    "id"            => $course_id,
                    "title"         => get_the_title( $course_id )
                );
                $cert_complete = intval( learndash_user_get_course_completed_date( $user->ID, $course_id ) );

                $cert = array(
                    "id"                        => $cert_id,
                    "title"                     => get_the_title( $cert_id ),
                    "required_course"           => $course,
                    "date_completed"            => $cert_complete,
                    "date_completed_formatted"  => ( new DateTime( "@$cert_complete" ) )->format( "F j, Y H:i:s" ),
                    "link"                      => $cert_url
                );

                $certs[] = $cert;
            }
        }

        if( !empty( $certs ) ) {
            $req_user_data["certificates"] = $certs;
        }
    }

    public function check_token( $request ) {
        /*
            Only allow the user to use the API if a valid token is given
            @params:
                $request            -> array
            @return:
                bool
        */

        $headers = array(
            "Authorization"     => $request->get_header( "Authorization" ),
            "Content-type"      => "application/json"
        );

        $pload = array(
            "headers"           => $headers
        );

        $jwt_url = home_url() . "/api/jwt-auth/v1/token/validate";

        $response = wp_remote_post( $jwt_url, $pload );

        $response = json_decode( $response["body"], true );

        return $response["code"] == "jwt_auth_valid_token";
    }

    public function get_users( $request ) {
        /*
            Get all users and their user meta, course data, and certificate data
            @params:
                $request            -> array
            @return:
                array( array( string    => string/array ) )
        */

        $this->prepare_sql_filters( $request );

        $user_data = array();
        $filters = $this->set_users_filters( $request );
        $get_users_args = array(
            "role__not_in" => array( "administrator" ),
            "meta_query"   => $filters,
            "number"       => $this->limit,
            "offset"       => $this->offset
        );

        $total_count = 0;

        if( isset( $request["last_login"] ) ) {
            $login_threshold = $this->create_date( $request["last_login"] );
            if( empty( $login_threshold ) ) {
                return new WP_Error("invalid_argument", "last_login contains an invalid argument");
            }
        }

        foreach( get_users( $get_users_args ) as $user ) {
            $session_tokens = get_user_meta( $user->ID, "session_tokens", true );
            $login_time = $this->get_last_login_time( $session_tokens );
            $run_user_creation = true;
            if( isset( $request["last_login"] ) && strtotime( $login_time ) < $login_threshold ) {
                $run_user_creation = false;
            }

            if( $run_user_creation == true ) {
                $user_activity = null;
                $user_activity = $this->get_learndash_activity( $request, $user->ID );
                if( !empty( $user_activity ) ) {
                    $req_user_data = null;
                    $this->get_basic_user_data( $req_user_data, $user, $login_time );
                    $this->get_course_user_data( $req_user_data, $user_activity );
                    $this->get_certificate_user_data( $req_user_data, $user );

                    $user_data[] = $req_user_data;
                }
            }
        }

        if( empty( $user_data ) ) {
            return new WP_Error( "empty_result", "No users found that matches arguments" );
        }
        return $user_data;
    }

    public function register_routes() {
        /*
            Register all the routes for the controller
        */

        register_rest_route( $this->namespace, "/users", array(
            // GET request
            array(
                "methods"               => WP_REST_Server::READABLE,
                "callback"              => array( $this, "get_users" ),
                "permission_callback"   => array( $this, "check_token" )
            )
        ));
    }

}
